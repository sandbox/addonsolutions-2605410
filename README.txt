paypal_method
~~~~~~~~~~~~

paypal_method is a drupal module to provide adaptive and
instalment paypal payment getway.

INSTALL
~~~~~~~

See the getting started guild on installing drupal modules:
http://drupal.org/getting-started/install-contrib/modules

USAGE
~~~~~

This module allows you to add handle adaptive and instalment
paypal payments in ubercart.

Step 1: Enable module on your drupal site.

Step 2: Setup adptive user for payment:
  * Go to:
    "Store administration" -> "Paypal" -> "Add paypal partners"
  * Check all paypal partners Go to :
    "Store administration" -> "Paypal" -> "Paypal partners emails"

Step 3: How to Create Instalment product in ubercart?
  * Go to:
    "Content" -> "Add Content" -> "product"
    On this content type you will get one field "Instalment Payment".


Credits
~~~~~~~
Addon solutions (http://www.addonsolutions.com/)

LICENSE
~~~~~~~
No guarantee is provided with this software, no matter how critical your
information, module authors are not responsible for damage caused by this
software or obligated in any way to correct problems you may experience.

This software licensed under the GNU General Public License 2.0.
http://www.gnu.org/licenses/gpl-2.0.txt.
