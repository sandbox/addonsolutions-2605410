<?php

/**
 * @file
 * Adaptive and instalment payment setting from admin.
 */

/**
 * Function for paypal_methods.
 * This function will redirect to paypal setting page.
 */
function paypal_methods() {
  drupal_goto('admin/store/settings/paypal');
}

/**
 * Function for paypal_methods info.
 */
function paypal_methods_info($form, &$form_state) {
  $methods = explode(',', variable_get('paypal_methods'));
  $form['paypal'] = array(
    '#type' => 'fieldset',
    '#title' => t('Paypal Methods'),
    '#weight' => 0,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['paypal']['methods'] = array(
    '#type' => 'checkboxes',
    '#options' => drupal_map_assoc(array(t('Adaptive'), t('Instalment'))),
    '#title' => t('Paypal Method'),
    '#default_value' => $methods,
  );
  $form['paypal']['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  return $form;
}

/**
 * Function for paypal_methods info submit.
 */
function paypal_methods_info_submit($form, &$form_state) {
  variable_set('paypal_methods', implode(',', $form_state['values']['methods']));
  drupal_set_message(t('Paypal Methods is updated.'));
}

/**
 * Function for paypal_methods adaptive emails.
 */
function paypal_methods_adaptive_emails($form, &$form_state) {
  $form['partners'] = array(
    '#type' => 'fieldset',
    '#title' => t('Partners emails'),
    // Set up the wrapper so that AJAX will be able to replace the fieldset.
    '#prefix' => '<div id="emails-fieldset-wrapper">',
    '#suffix' => '</div>',
  );

  $form['partners']['emails'] = array(
    '#type' => 'textfield',
    '#title' => t('Paypal Email'),
  );
  $form['partners']['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount %'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Function for paypal_methods adaptive emails validation.
 */
function paypal_methods_adaptive_emails_validate($form, &$form_state) {
  $allamount = db_query("SELECT SUM(payment_per) as total FROM {paypal_partners}")->fetchObject();
  if ($allamount->total + $form_state['values']['amount'] > 100) {
    form_set_error('amount', t('You can not add more the 100%.'));
  }
}

/**
 * Function for paypal_methods adaptive emails submit.
 */
function paypal_methods_adaptive_emails_submit($form, &$form_state) {
  $insert = db_insert('paypal_partners')
    ->fields(array(
      'email' => $form_state['values']['emails'],
      'payment_per' => $form_state['values']['amount'],
    ))->execute();
  if ($insert) {
    drupal_set_message(t('Paypal email is inserted.'));
  }
}

/**
 * Function for paypal_methods paypal partners edit submit.
 */
function paypal_methods_all_paypal_partners_edit_submit($form, $form_state,$pid) {
  $email_update = db_update('paypal_partners')
    ->fields(array(
      'email' => $form_state['values']['emails'],
      'payment_per' => $form_state['values']['amount'],
    ))
    ->condition('pid', $pid, '=')
    ->execute();
  if ($email_update) {
    drupal_set_message(t('Paypal Partner detail is updated.'));
  }
}

/**
 * Function for paypal_methods paypal partners edit validation.
 */
function paypal_methods_all_paypal_partners_edit_validate($form, $form_state) {
  $allamount = db_query("SELECT SUM(payment_per) as total FROM {paypal_partners}")->fetchObject();
  if ($allamount->total + $form_state['values']['amount'] > 100) {
    form_set_error('amount', t('You can not add more the 100%.'));
  }
}

/**
 * Function for paypal_methods paypal partners edit.
 */
function paypal_methods_all_paypal_partners_edit($form, $form_state) {
  $getdetail = db_query("SELECT * FROM {paypal_partners} WHERE pid=:pid", array(':pid' => arg(4)))->fetchObject();
  $form['partners'] = array(
    '#type' => 'fieldset',
    '#title' => t('Partners emails'),
    // Set up the wrapper so that AJAX will be able to replace the fieldset.
    '#prefix' => '<div id="emails-fieldset-wrapper">',
    '#suffix' => '</div>',
  );

  $form['partners']['emails'] = array(
    '#type' => 'textfield',
    '#title' => t('Paypal Email'),
    '#default_value' => $getdetail->email,
  );
  $form['partners']['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount %'),
    '#default_value' => round($getdetail->payment_per),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Edit'),
  );
  return $form;
}

/**
 * Function for paypal_methods paypal partners delete.
 */
function paypal_methods_all_paypal_partners_delete($form, $form_state,$pid) {
  $getdetail = db_query("SELECT * FROM {paypal_partners} WHERE pid=:pid", array(':pid' => $pid))->fetchObject();
  return confirm_form($form,
    t('Are you sure you want to delete %email?', array('%email' => $getdetail->email)),
    'admin/store/paypal/partners_list/' . $getdetail->pid,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Function for paypal_methods paypal partners delete submit.
 */
function paypal_methods_all_paypal_partners_delete_submit($form, $form_state) {
  $delete_email = db_delete('paypal_partners')
    ->condition('pid', arg(4))
    ->execute();
  if ($delete_email) {
    drupal_set_message(t('Paypal Partnet email is deleted.'));
  }
}

/**
 * Function for paypal_methods all paypal partners.
 */
function paypal_methods_all_paypal_partners($form, $form_state) {
  $header = array(
    'email' => array('data' => t('Email'), 'field' => 'p.email'),
    'payment_per' => array('data' => t('Amount (%)'), 'field' => 'p.payment_per'),
  );
  $header['operations'] = array('data' => t('Operations'));
  // We are extending the PagerDefault class here.
  // It has a default of 10 rows per page.
  // The extend('PagerDefault') part here does all the magic.
  $query = db_select('paypal_partners', 'p')->extend('PagerDefault');
  $query->fields('p', array('pid', 'email', 'payment_per'));

  // Change the number of rows with the limit() call.
  $result = $query
    ->limit(10)
    ->orderBy('p.pid')
    ->execute();
  $destination = drupal_get_destination();
  $options = array();
  foreach ($result as $row) {
    $options[$row->pid] = array(
      'email' => check_plain($row->email),
      'payment_per' => round($row->payment_per),
    );
    $operations = array();

    $operations['edit'] = array(
      'title' => t('edit'),
      'href' => 'admin/store/paypal/partners_list/' . $row->pid . '/edit',
      'query' => $destination,
    );
    $operations['delete'] = array(
      'title' => t('delete'),
      'href' => 'admin/store/paypal/partners_list/' . $row->pid . '/delete',
      'query' => $destination,
    );
    $options[$row->pid]['operations'] = array();
    $options[$row->pid]['operations'] = array(
      'data' => array(
        '#theme' => 'links__node_operations',
        '#links' => $operations,
        '#attributes' => array('class' => array('links', 'inline')),
      ),
    );
  }
  $form['nodes'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No content available.'),
  );
  $form['pager'] = array(
    '#theme' => 'pager',
    '#element' => 0,
    '#weight' => 5,
  );
  return $form;
}
