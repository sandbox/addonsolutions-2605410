<?php

/**
 * @file
 * Install, update and uninstall functions for the paypal_methods module.
 */

/**
 * Implements hook_foo().
 */
function paypal_methods_schema() {
  $schema['paypal_partners'] = array(
    'description' => 'Paypal partners email addresses for Adaptive paypal payment method.',
    'fields' => array(
      'pid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'email' => array(
        'description' => 'paypal partners email.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '0',
      ),
      'payment_per' => array(
        'description' => 'The amount of the Partners users.',
        'type' => 'numeric',
        'precision' => 15,
        'scale' => 3,
        'not null' => TRUE,
        'default' => 0.0,
      ),
    ),
    'primary key' => array('pid'),
  );
  $schema['paypal_adptive_detail'] = array(
    'description' => 'Paypal Adaptive detail.',
    'fields' => array(
      'order_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'detail' => array(
        'description' => 'paypal partners email.',
        'type' => 'text',
        'not null' => TRUE,
      ),
    ),
  );
  $schema['paypal_instalment_detail'] = array(
    'description' => 'Paypal Instalment detail.',
    'fields' => array(
      'pid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'detail' => array(
        'description' => 'paypal instalments.',
        'type' => 'text',
        'not null' => TRUE,
      ),
    ),
  );
  $schema['instalment_order_detail'] = array(
    'description' => 'Instalment order detail.',
    'fields' => array(
      'order_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'next_charge' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'fee_amount' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'remaining_intervals' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'charged_intervals' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'created' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'order_product_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'module' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ),
      'preapproval_key' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
    ),
  );
  return $schema;
}

/**
 * Implements hook_install().
 */
function paypal_methods_install() {
  $attribute = new stdClass();
  $attribute->name = 'paypal_instalment_option';
  $attribute->label = 'Payment Type';
  $attribute->ordering = 0;
  $attribute->required = 1;
  $attribute->display = 2;
  $attribute->description = 'Payment Type ( Full / Instalment )';
  uc_attribute_save($attribute);
  variable_set('paypal_instalment_option', $attribute->aid);

  $option = new stdClass();
  $option->aid = $attribute->aid;
  $option->name = 'Full Payment';
  uc_attribute_option_save($option);

  $option2 = new stdClass();
  $option2->aid = $attribute->aid;
  $option2->name = 'Instalment Payment';
  uc_attribute_option_save($option2);
}

/**
 * Implements hook_uninstall().
 */
function paypal_methods_uninstall() {
  variable_del('uc_adaptive_paypal_address_selection');
  variable_del('paypal_methods');
  variable_del('paypal_methods_account_email');
  variable_del('uc_adaptive_server');
  variable_del('paypal_methods_paypal_address_selection');
  variable_del('paypal_methods_paypal_language');
  variable_del('paypal_methods_paypal_address_override');
  variable_del('paypal_methods_paypal_email');
  variable_del('paypal_methods_paypal_password');
  variable_del('paypal_methods_paypal_sign');
  variable_del('paypal_methods_paypal_appid');
  variable_del('paypal_methods_paypal_mp_mode');
}
